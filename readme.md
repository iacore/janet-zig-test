This is a simple Zig program that runs some Janet code and fetch data from it.

Here's [an article by me](https://www.1a-insec.net/blog/16-buy-janet-get-gc-free/) describing how this works. However, it may be easier for you to read the code directly.

The Janet-Zig interface is provided by [jzignet](https://github.com/greenfork/jzignet/).

## Building

```
zig build
zig build run # run program
```
