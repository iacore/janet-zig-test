const std = @import("std");
const janet = @import("jzignet");

pub fn main() !void {
    try janet.init();
    defer janet.deinit();

    const env = janet.Environment.init(null);

    _ = try env.doBytes(@embedFile("catalog.janet"), "catalog.janet");

    const catalog = env.toTable().get(janet.symbol("catalog")).?.get(janet.keyword("value")).?;

    // another way:
    // const catalog = env.doBytes("catalog");

    // not working
    // const binding = env.resolveExt("catalog");
    // std.log.info("binding type: {}", .{binding.type});  // is NONE
    // const catalog = janet.Janet.fromC(binding.value);

    // another way:
    // const catalog = env. envLookup().toTable().get(janet.symbol("catalog")).?;

    const len = catalog.length();

    const stdout = std.io.getStdOut().writer();
    try stdout.print("len: {}\n", .{len});

    const CatalogItem = struct {
        name: janet.String,
        quantity: i32,
        unit_price: ?i32,
        fn print(this: @This()) !void {
            const out = std.io.getStdOut().writer();
            try out.print("{s} x {}", .{ this.name.slice, this.quantity });
            if (this.unit_price) |price|
                try out.print(", {?}€ ea", .{price});
            try out.writeByte('\n');
        }
    };
    var i: i32 = 0;

    while (i < len) : (i += 1) {
        const stuff = catalog.getIndex(i).?;
        const item = CatalogItem{
            .name = try stuff.get(janet.keyword("name")).?.unwrap(janet.String),
            .quantity = try stuff.get(janet.keyword("quantity")).?.unwrap(i32),
            .unit_price = if (stuff.get(janet.keyword("unit_price"))) |x| try x.unwrap(i32) else null,
        };
        try item.print();
    }
}
