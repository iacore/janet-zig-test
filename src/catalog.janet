(def catalog [
    {:name "a" :quantity 1 :unit_price 200}
    {:name "b" :quantity 5 :unit_price 300}
    {:name "c" :quantity 10}
    {:name "d" :quantity 8}
    {:name "e" :quantity 2}
])
